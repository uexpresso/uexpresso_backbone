define(['../backbone','../Api', 'model/sweeperProperty'], function (Backbone, DbApi, SweeperProperty) {

	var RequisitionVisit= Backbone.Model.extend({
		defaults:function(){
			return{
				id:"0",
				location:'', position:'',
				bbr_commune_code:'', bbr_property_number:'', bbr_code:'',
				zip:'', placename:'', address:'', house:'',
				title:'', fullname:'',				
				requisitionId:"0",
				propertyId:"0",
				planned:"0000-00-00",
				notified:"0000-00-00",
				completed:"0000-00-00",
				created:"0000-00-00",
				editor:"0",
				sweepingDate:"0000-00-00",
				hasRest:"0",
				isCancelled:"0",
				comment:""
			};
		},
		parse: function(responce,options) {	

			if(this.attributes.Property!=undefined) this.attributes.Property.set(responce);
			else  this.attributes.Property=new SweeperProperty(responce);
			/*jQuery('#propertyTable').append('<tr>'+
									'<td>'+responce.location+responce.position+'</td>'+
									'<td>'+responce.bbr_commune_code+responce.bbr_property_number+responce.bbr_code+'</td>'+
									'<td>'+responce.zip+' '+responce.placename+' '+responce.address+' '+responce.house+'</td>'+
									'<td>'+responce.title+' '+responce.fullname+'</td>'+
									'<td>'+responce.planned+'</td>'+
									'<td>'+responce.notified+'</td>'+
									'<td>'+responce.completed+'</td>'+
			'</tr>');
			jQuery('#requisitionsTable').hide();
			jQuery('#propertyBlock').show();*/
			return responce;
		},
		objectName: "RequisitionVisit",
		sync:function(){ return false; },		
		logThis:function(){console.log(this);}
	});


return RequisitionVisit;
});