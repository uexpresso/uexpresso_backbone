define(['../backbone','../Api'], function (Backbone) {

	var RequisitionVisitView=Backbone.View.extend({
		tagName:"tr",
		objectName: "RequisitionVisitView",
		template: _.template('<td><%- location %><%- position %></td>'+
									'<td><%- bbr_commune_code %><%- bbr_property_number %><%- bbr_code %></td>'+
									'<td><%- zip %> <%- placename %> <%- address %> <%- house %></td>'+
									'<td><%- title %> <%- fullname %></td>'+
									'<td><%- planned %></td>'+
									'<td><%- notified %></td>'+
									'<td><%- completed %></td>'),
		events:{
			
		},
		render: function() {
			this.$el.html(this.template(this.model.toJSON()));
			this.model.tableRow=this.$el[0];
			this.$el[0].model=this.model;
			
			var self=this;						
			return this.$el;
		}
	});


return RequisitionVisitView;
});