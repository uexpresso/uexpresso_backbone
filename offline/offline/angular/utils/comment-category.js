fejerproApp.directive('comment', function () {
	return {
		restrict: 'E',
		replace: true,
		link: function(scope, element, attr) {
			scope.getContentUrl = function() {
                if(attr.category != 'ANN' && attr.category != 'OPK')
					return 'offline/angular/views/comment-default.html';
				else
					return 'offline/angular/views/comment-'+attr.category+'.html';
			}
       },
       template: '<span ng-include="getContentUrl()"></span>'
	}
});