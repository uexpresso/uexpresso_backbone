define(['backbone'], function (Backbone) {


/**
	backbone sync method is overrided here to adapt script to Fejerpro main api
**/
Backbone.sync = function(method, model, options) {
	
	/**
		only use POST just for case
	**/
    var type = "POST"; //methodMap[method];
	
    // Default options, unless specified.
    _.defaults(options || (options = {}), {
      emulateHTTP: Backbone.emulateHTTP,
      emulateJSON: Backbone.emulateJSON
    });

    // Default JSON-request options.
    var params = {type: type, dataType: 'json'};
	
    // Ensure that we have a URL.
	
	if(model.url==undefined) model.url=model.objectName;
	
	/**
		a tweaked url here
	**/
    if (!options.url) {
      params.url =  ('/backbone/'+_.result(model, 'url')+"/action,"+method+"/request,ajax/mode,json/") || urlError();
    }
	
	

    // Ensure that we have the appropriate request data.
    if (options.data == null && model && (method === 'create' || method === 'update' || method === 'patch')) {
      params.contentType = 'application/json';
      params.data = JSON.stringify(options.attrs || model.toJSON(options));
    }

    // For older servers, emulate JSON by encoding the request into an HTML-form.
    if (options.emulateJSON) {
      params.contentType = 'application/x-www-form-urlencoded';
      params.data = params.data ? {model: params.data} : {};
    }
	
	if(model.sendAllways!=undefined)
	{
		if(params.data==undefined) params.data="";
		for(x in model.sendAllways)
		{
			params.data+="&form["+model.sendAllways[x]+"]="+_.result(model, model.sendAllways[x]);
		}
	}


    // For older servers, emulate HTTP by mimicking the HTTP method with `_method`
    // And an `X-HTTP-Method-Override` header.
    if (options.emulateHTTP && (type === 'PUT' || type === 'DELETE' || type === 'PATCH')) {
      params.type = 'POST';
      if (options.emulateJSON) params.data._method = type;
      var beforeSend = options.beforeSend;
      options.beforeSend = function(xhr) {
        xhr.setRequestHeader('X-HTTP-Method-Override', type);
        if (beforeSend) return beforeSend.apply(this, arguments);
      };
    }

    // Don't process data on a non-GET request.
    if (params.type !== 'GET' && !options.emulateJSON) {
      params.processData = false;
    }

    // If we're sending a `PATCH` request, and we're in an old Internet Explorer
    // that still has ActiveX enabled by default, override jQuery to use that
    // for XHR instead. Remove this line when jQuery supports `PATCH` on IE8.
    if (params.type === 'PATCH' && noXhrPatch) {
      params.xhr = function() {
        return new ActiveXObject("Microsoft.XMLHTTP");
      };
    }

    // Make the request, allowing the user to override any Ajax options.
    var xhr = options.xhr = Backbone.ajax(_.extend(params, options));
    model.trigger('request', model, xhr, options);
    return xhr;
  };

 
	/**
		DbApi - an adapter object for using ydn.db
		this stuff exports an instance of ydn.db.Storage object
		
		any changes to db schema should be put here
	**/
	var DbApi=function(){
		
		var db_schema = {
			fullTextCatalogs: [{
			  name: 'requisitionId',
			  lang: 'en',
			  sources: [
				{
				  storeName: 'requisition_visits',
				  keyPath: 'requisitionId',
				  weight: 1.0
				}]
			},{
			  name: 'requisition_visits_location',
			  lang: 'en',
			  sources: [
				{
				  storeName: 'requisition_visits',
				  keyPath: 'location',
				  weight: 1.0
				}]
			},{
			  name: 'requisition_visits_position',
			  lang: 'en',
			  sources: [
				{
				  storeName: 'requisition_visits',
				  keyPath: 'position',
				  weight: 1.0
				}]
			},{
			  name: 'requisition_visits_tourposition',
			  lang: 'en',
			  sources: [
				{
				  storeName: 'requisition_visits',
				  keyPath: ['location','position'],
				  weight: 1.0
				},
				{
				  storeName: 'requisition_visits',
				  keyPath: 'position',
				  weight: 0.5
				}]
			},{
			  name: 'requisition_visits_address',
			  lang: 'en',
			  sources: [
				{
				  storeName: 'requisition_visits',
				  keyPath: 'address',
				  weight: 1.0
				}]
			}],
			stores: [
				{
					name: 'requisitions',
					keyPath: 'id'
				},
				{
					name: 'requisition_visits',
					keyPath: ['id', 'requisitionId'],
					indexes: [{
					   keyPath: 'requisitionId', // required.
					   name: 'requisitionId',      // usually omitted. generally same as keyPath.
					   unique: false,      // unique constrain
					   multiEntry: true   //
					}]
				}
			]
		};
	
		var dbname="FejerPro";		
		Db = new ydn.db.Storage(dbname,db_schema);
				
		
		return Db;
	
	};
	
	
	/**
		a "cunning" stuff for sorting an object collections by id as by default it tries to use id as a string (facepalm)
		
		this is also used in a nice method orderBy() for Collection object described below
	**/
	Backbone.Collection.prototype.comparator= function(a, b) {
    // Assuming that the sort_key values can be compared with '>' and '<',
    // modifying this to account for extra processing on the sort_key model
    // attributes is fairly straight forward.
		
		var result=0;
		var orderInt=1;
		
		if(this.order=='desc')  orderInt=-1;

		if(this.sort_key!=undefined)
		{
		    a = a.get(this.sort_key);
			b = b.get(this.sort_key);
			result= a > b ?  1*orderInt
				 : a < b ? -1*orderInt
				 :          0;
		}
		
		a = parseInt(a.get("id"));
		b = parseInt(b.get("id"));
		result= a > b ?  1*orderInt
			 : a < b ? -1*orderInt
			 :          0;
			
		return result;	 
	}

	/**
		a method to run re-sort of a collection by some key. 
		by default it orders a collection by id ascending.
		
		if collection has to be orderer by default by other key or in descending direction - it can be specified in collection definition with "order:'desc'" and "sort_key:'somefield'" options
	**/
	Backbone.Collection.prototype.orderBy=function(key, order){
		if(key!=undefined) this.sort_key=key;
		else this.sort_key="id";
		
		if(order=='asc' || order=='ASC' )  this.order='asc'; 
		else if(order=='desc' || order=='DESC')  this.order='desc';	
		
		this.sort();
	}
	
	
	/**
		a somehow crappy but still universal method to refresh a collection objects
		the run of it requires adjustments in /db/class.BackboneQuires.php as by default it will assume that data table requires no joins and has sweeper_id field
		
		method will send an array of ids and modified dates of objects stored in IndexedDb and reflected in collection to the server
		responce is an array of modified or inserted objects that will be inserted to Collection
		
		for each of renewed models render() method is called
		if model is new renderNew() called, this method HAS to be specified in /model/objectname.js file
		
		method REQUIRES that collection has 'table' attribute specified AND DbApi (instance of DbApi) to be inserted as argument  (double facepalm here)
	**/
	Backbone.Collection.prototype.refresh=function(DbApi){
		var href="/backbone/refresh/filters,table,"+this.table+"/request,ajax/mode,json/&param="+new Date().getTime();
		
		var statedata=[];
		for(x in this.models)
		{			
		
			statedata.push({"id":this.models[x].id, "modified":this.models[x].get('modified')});					
		}
		var self=this;
		jQuery.ajax({
				type: 'POST',
				url: href,
				async: true,
				dataType: 'json',
				data: {"form":{"data":statedata}},
				timeout: 3000,
				error: function(err){
					console.log(err);
				},				
				complete: function(jxhr,status) {
					return null;
					if(status == "timeout"){
						console.log(status);
					}
					
					
				},
				success: function(responce) {					
					
					var updates=responce.result;
					if(responce.result!=undefined)
					{
						if(updates.length!=0)
						{
							var updatedModels=self.set(updates, {remove: false});
	
							if(updatedModels.length!=0) for(x in updatedModels) 
							{
							/** for new objects **/
							if(updatedModels[x].View==undefined) updatedModels[x].renderNew();
							/** for changed objects **/
							else updatedModels[x].View.render();
							
							}
							/** save to IndexedDb **/
							DbApi.put(self.table, updates);
						}
					}
				}
			});
		
	} 
	
	var Request= Backbone.Model.extend({
		defaults:function(){
			return {
				creator:{}, //a backbone object for callbacks scope
				queryType:'post',
				href:'/',
				queryDataType:'json',
				data:{"form":{}},
				doneCallback:function(){},
				errorCallback:function(){},
				waitingCallback:function(){},
				created:new Date(),
				status:'ready' // ready done pending error
			};
		},
		objectName: "Request",
		sync:function(){ return false; },		
		logThis:function(){console.log(this);},
		
		send:function(){
				var self=this;
				this.set('status','pending');
				var url = this.get('href')+"&param="+new Date().getTime();
				jQuery.ajax({
					type: self.get('queryType'),
					url: url,
					async: true,
					dataType: self.get('queryDataType'),
					data: self.get('data'),
					timeout: 3000,
					error: function(err){
						
					},
					success: function(data) {
				
						self.set('status','done');
						if (data['code'] == 0) self.attributes.errorCallback.call(self.attributes.creator,data);
						else self.attributes.doneCallback.call(self.attributes.creator,data);
						RequestQueue.remove(self);
					},
					complete: function(jxhr,status) {
			
						if(status == "timeout" || status == "parsererror")
						{			
							self.attributes.waitingCallback.call(self.attributes.creator);
							self.set('status','ready');
												
						}
						else  if(status == "success")
						{
							self.set('status','done');
						}
						else
						{
							self.set('status','error');
							self.attributes.errorCallback.call(self.attributes.creator,err);
							RequestQueue.remove(self);	
						}		
					}
				});	
		}
	});
	
	var RequestQueue = Backbone.Collection.extend({
		model:Request,
		objectName: "RequestQueue", /** unused. for debug only **/				
		isAuto:false,
		isConnected:true,
		timer:false,
		
		clean:function(){	
			this.reset();
			return this;
		},		
		sendAll:function(){	
			for(x in this.models)
			{
				if(this.models[x].get('status')=='ready') this.models[x].send();					
			}
		},		
		connect:function(){	
			var self=this;
			self.isConnected=false;
			if(this.length)
			{			
				jQuery.get('/robots.txt'+"&param="+new Date().getTime(),function(e){
					self.isConnected=true;
					self.sendAll();
				});
			} else self.clearTimer();
		},		
		setAutoConnect:function(isAuto){
			this.isAuto=isAuto;
			if(this.isAuto && !this.timer) this.startTimer();
		},		
		startTimer:function(){	
			if(this.timer) return;
			var self=this;
			this.timer=setInterval(function(){self.connect();}, 10000);
		},		
		clearTimer:function(){
			if(!this.timer) return;
			clearInterval(this.timer);
		}
	});

	var RequestQueue=new RequestQueue();
	var DbApi=new DbApi();
	DbApi.RequestQueue=RequestQueue; //for testing
	/**
	Model->requestAjax(args[])->/has connection?/->yes->sendAjax(args)->return result
	|->no->requestsCollection.add(requestObj).startTimer() 
	
	send('/somewhere', {something}).done(function(result){dosmsth();})
	**/
	Backbone.Collection.prototype.send=
	Backbone.View.prototype.send=
	Backbone.Model.prototype.send= function(qUrl, data, waitingCallback){
		var creator=this;
		this.waitingCallback=waitingCallback;
		var promise= new Promise(function(resolve, reject)
			{
				var self=this;
				var answer;
				var qType='post';
				var qDataType='json';
				var sendArr=data;
				if(data==undefined) 
				{
					qType='get';
					sendArr={};
					qDataType='html';
				}
				if(typeof(this.waitingCallback)=='function') this.waitingCallback.call(creator);
				var url = qUrl+"&param="+new Date().getTime();
				jQuery.ajax({
					type: qType,
					url: url,
					async: true,
					dataType: qDataType,
					data: sendArr,
					timeout: 3000,
					error: function(err){
					
					},
					success: function(data) {					
						if (data['code'] == 0) reject(data);
						else resolve(data);
					},
					complete: function(jxhr,status) {
					
						if(status == "timeout" || status == "parsererror")
						{			
							var Request=RequestQueue.add({
								creator:creator, //a backbone object for callbacks scope
								queryType:qType,
								href:url,
								queryDataType:qDataType,
								data:sendArr,
								doneCallback:promise.doneCallback,
								errorCallback:promise.errorCallback,
								waitingCallback:promise.waitingCallback
							});
							RequestQueue.isConnected=false;
							RequestQueue.setAutoConnect(true);
							promise.waitingCallback.call(creator);
						}
						else  if(status == "success")
						{
							promise.doneCallback.call(creator);
						}
						else
						{
							promise.errorCallback.call(creator);
						}		
						return null;
					}
				});			
				
			});
			
			promise.done=function(callback)
			{	
				this.doneCallback=callback;
				this.then(function(result){
					callback.call(creator, result);
				});
				return this;
				
			}
			
			promise.waiting=function(callback)
			{
				this.waitingCallback=callback;
				return this;
			}
			
			promise.error=function(callback)
			{
				this.errorCallback=callback;
				this.then(function(){},function(result){callback.call(creator, result);});
				return this;
			}
		
			return promise;
		};
		
	
	
	
	
	
	/** this returns a created instance of DbApi object to outside of scope. for main.js in this case **/
	return DbApi;
});