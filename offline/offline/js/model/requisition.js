define(['../backbone','../Api', 'collection/visits', 'view/requisition'], function (Backbone, DbApi, RequisitionVisitsList, RequisitionView) {

	var Requisition=Backbone.Model.extend({
		defaults:function() {			
			return {
				id:"0",
				number:"0",
				req_number:'0',
				property_group:'',
				tours:"0",
				year:"2014",					
				propertyGroup:"ORD",
				comment:"test requisition",				   
				isCompleted:"false",
				printed:"12/12/2014",
				created:"12/12/2014",
				visitNumber:"1/1",
				visitsList: this.setVisitList(),
				visitsCount:0
			};
		},	
		setVisitList:function(newVisitsList){
			if( this.visitsList==undefined) 
				return new RequisitionVisitsList();
			if(newVisitsList!=undefined) this.visitsList=newVisitsList;
			this.visitsList.requisitionId=this.id;
		},
		getVisits:function(){		
			var self=this;		
			
			if( this.visitsList==undefined) 
			{
				this.visitsList=new RequisitionVisitsList();
				this.visitsList.requisitionId=this.id;
			}
			var VisitsList=this.visitsList;
			
			DbApi.count('requisition_visits', 'requisitionId',  ydn.db.KeyRange.only(self.id.toString())).done(	function(result) {
				self.set('visitsCount',result);
				
				if(result==0)
				{
					
					jQuery(self.tableRow).find('.editButton').hide().after('<div class="loader" style="width:100%; text-align:center;"><img src="/img/ajaxLoader.gif" style="height:25px;"/></div>');					
					self.visitsList.fetch({success:function(){						
						jQuery('#requisitionsTable').hide();
						jQuery('#propertyTable tbody').empty();
						jQuery('#propertyBlock').show();
						
						jQuery(self.tableRow).find('.editButton').html('Show visits').show();
						jQuery(self.tableRow).find('.loader').remove();
						var modelArray=[];
						for(x in self.visitsList.models)
						{
							modelArray.push(self.visitsList.models[x].toJSON());					
						}
						DbApi.add('requisition_visits', modelArray).done(function(request){self.updateVisitsCount();});
						self.visitsList.appendListTo('#propertyTable tbody');
					}});
				}
				else
				{					
					jQuery('#requisitionsTable').hide();
					jQuery('#propertyTable tbody').empty();
					jQuery('#propertyBlock').show();
					
					DbApi.values('requisition_visits', 'requisitionId',  ydn.db.KeyRange.only(self.id.toString())).done(	function(result) {						
						self.visitsList.add(result); 						
						self.visitsList.appendListTo('#propertyTable tbody');
					});
				}				
			});
			
			
		
			
		},
		updateVisitsCount:function(){
			var self=this;
			
			DbApi.count('requisition_visits', 'requisitionId',  ydn.db.KeyRange.only(self.id.toString())).done(	function(result) {
				self.set('visitsCount',result);
				if(result>0) jQuery(self.tableRow).find('.editButton').html('Show visits');
				jQuery(self.tableRow).find('.editButton').show();
				jQuery(self.tableRow).find('.visitsCountCell').html(result);
			});
		},
		/** this is an entersitng stuff to add-hock place a new model to the requisitions table **/
		renderNew:function(){
			this.prependObjectTo('#requisitionsTable tbody');
		},
		prependObjectTo:function(jObject){
			var View=new RequisitionView({model: this});	
			$(View.render().el).prependTo(jObject);
		},
		
		placeEditButton:function(){
			jQuery(this.tableRow).find('.editButton').show();
		},
		objectName: "Requisition",  /** unused. for debug only **/
		sync:function(){ return false; }, /** kill of default method **/	
		logThis:function(){console.log(this);}  /** unused. for debug only **/
	});


return Requisition;
});