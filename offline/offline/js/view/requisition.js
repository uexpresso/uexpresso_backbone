define(['../backbone','../Api'], function (Backbone, DbApi) {

	/**
		View object is mostly used to define HTML output of a model along with behaviour of buttons and other control stuff
	**/
	var RequisitionView=Backbone.View.extend({
		tagName:"tr",
		objectName: "RequisitionView",  /** unused. for debug only **/
		templateName:"RequisitionsList_node",
		template: _.template(jQuery('#RequisitionList_node').html()),
		events:{
			"click .editButton" : function(e){
				this.model.getVisits(); /** hardcoded behaviour of a button right to the requisition data **/
			},
			"click .editDescription" : function(e){			
				e.preventDefault();
				var jObj=e.currentTarget;
				jQuery(jObj).hide();
				jQuery(jObj).parent().find('.currentDescription').hide();
				jQuery(jObj).parent().find('.desriptionInput').show().focus();
			},			
			"click .completeButton" : function(e){	
				
				e.preventDefault();				
				var jObj=e.currentTarget;
				jQuery(jObj).css('color','#ccc');
				var self=this;
				DbApi.applyTemplate('Requisition_page','requisitions');
				
			},
			"blur .desriptionInput": function(e){
				var jObj=e.currentTarget;
				jQuery(jObj).parent().find('.editDescription').show();
				var newText=jQuery(jObj).val();
				jQuery(jObj).parent().find('.currentDescription').text(newText).show();
				jQuery(jObj).hide();
				this.model.set('comment' , newText);
				DbApi.put('requisitions', this.model.toJSON());
				
				
				var responce=this.send('/requisitions/saveRequisitionDescription/node,'+this.model.get('id')+'/request,ajax/mode,json/',{form:{comment:newText}})
				.done(function(r){
					jQuery(jObj).parent().find('.currentDescription').css('color','#0F0');
					
				})
				.error(function(e){
					jQuery(jObj).parent().find('.currentDescription').css('color','#F00');
				
				})
				.waiting(function(){
					jQuery(jObj).parent().find('.currentDescription').css('color','#ccc');
				
				});
				
			},
			"keydown .desriptionInput": function(e){
				var jObj=e.currentTarget;
				if(e.keyCode=='13' || e.keyCode=='13') jQuery(jObj).blur();
			}
		},
		render: function(totalLength) {
			//this.getTemplate(function(templateData){				
				
				//this.template=_.template(templateData);
				this.$el.html(this.template(this.model.toJSON()));
				this.model.tableRow=this.$el[0];
				this.$el[0].model=this.model;
				this.model.View=this;
				var self=this;
			//});
			return this;		
			
			/** this is a genuine pain in an arse consisted of request to IndexedDb to count requisition visits and placing a "Show visits" if available in db**/
			/*if(totalLength!=undefined) {
				DbApi.count('requisition_visits', 'requisitionId',  ydn.db.KeyRange.only(self.model.id.toString())).done(	function(result) {
					self.model.set('visitsCount',result);
					if(result>0) jQuery(self.$el[0]).find('.editButton').html('Show&nbsp;visits');
					jQuery(self.$el[0]).find('.editButton').show();
				
					jQuery(self.$el[0]).find('.visitsCountCell').html(result);				
					var currentresult=(jQuery( "#progressbar" ).progressbar( "value")/100)*totalLength;
					currentresult++;
					jQuery( "#progressbar" ).progressbar( "value", (currentresult/totalLength)*100 );
					if(currentresult>=totalLength) jQuery( "#progressbar" ).fadeOut();
				});
			}
			
			
			return this;*/
		}
	});


return RequisitionView;
});