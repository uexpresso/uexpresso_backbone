define(['Api', 'Lang'], function (Api, Lang)  {
	
	function Controller(){
		return this;
	}
    Controller.prototype = {
        initialize: function(){
            // summary:
            //      Initializes the singleton.
            
            
        },
		Api:Api,
		Lang:Lang,
		lang:'basic',
		defaultFunction:function(){},
		
		template:function(templateName){
			var self=this;
			if(templateName==undefined) return;
			jQuery('.content').empty();
			
			jQuery.get('/offline/js/templates/'+templateName+'.jst',function(html){
			
				Lang.translateHtml(html, self.lang, function(html){ 
					jQuery('.content').html(html); 
				});
					
			});	
		},
		
		request:function(qUrl, data, waitingCallback){
			var creator=this;
			this.waitingCallback=waitingCallback;
			var promise= new Promise(function(resolve, reject)
				{
					var self=this;
					var answer;
					var qType='post';
					var qDataType='json';
					var sendArr=data;
					if(data==undefined) 
					{
						qType='get';
						sendArr={};
						qDataType='html';
					}
					if(typeof(this.waitingCallback)=='function') this.waitingCallback.call(creator);
					var url = qUrl+"&param="+new Date().getTime();
					jQuery.ajax({
						type: qType,
						url: url,
						async: true,
						dataType: qDataType,
						data: sendArr,
						timeout: 3000,
						error: function(err){
						
						},
						success: function(data) {					
							if (data['code'] == 0) reject(data);
							else resolve(data);
						},
						complete: function(jxhr,status) {
						
							if(status == "timeout" || status == "parsererror")
							{			
								var Request=RequestQueue.add({
									creator:creator, //a backbone object for callbacks scope
									queryType:qType,
									href:url,
									queryDataType:qDataType,
									data:sendArr,
									doneCallback:promise.doneCallback,
									errorCallback:promise.errorCallback,
									waitingCallback:promise.waitingCallback
								});
								RequestQueue.isConnected=false;
								RequestQueue.setAutoConnect(true);
								if(promise.waitingCallback!=undefined) promise.waitingCallback.call(creator);
							}
							else  if(status == "success")
							{
								if(promise.doneCallback!=undefined) promise.doneCallback.call(creator);
							}
							else
							{
								if(promise.errorCallback!=undefined) promise.errorCallback.call(creator);
							}		
							return null;
						}
					});			
					
				});
				
				promise.done=function(callback)
				{	
					this.doneCallback=callback;
					this.then(function(result){
						callback.call(creator, JSON.parse(result));
					});
					return this;
					
				}
				
				promise.waiting=function(callback)
				{
					this.waitingCallback=callback;
					return this;
				}
				
				promise.error=function(callback)
				{
					this.errorCallback=callback;
					this.then(function(){},function(result){callback.call(creator, JSON.parse(result));});
					return this;
				}
			
				return promise;
		}
    };
    

    

/*

	var Controller={
		
		
		
		
		
		extend : function(protoProps, staticProps) {
			var parent = this;
			var child;

			// The constructor function for the new subclass is either defined by you
			// (the "constructor" property in your `extend` definition), or defaulted
			// by us to simply call the parent's constructor.
			if (protoProps && _.has(protoProps, 'constructor')) {
			  child = protoProps.constructor;
			} else {
			  child = function(){ return parent.apply(this, arguments); };
			}

			// Add static properties to the constructor function, if supplied.
			_.extend(child, parent, staticProps);

			// Set the prototype chain to inherit from `parent`, without calling
			// `parent`'s constructor function.
			var Surrogate = function(){ this.constructor = child; };
			Surrogate.prototype = parent.prototype;
			child.prototype = new Surrogate;

			// Add prototype properties (instance properties) to the subclass,
			// if supplied.
			if (protoProps) _.extend(child.prototype, protoProps);

			// Set a convenience property in case the parent's prototype is needed
			// later.
			child.__super__ = parent.prototype;

			return child;
		}
	}
	
	(function(){
		var promises = {};
		 
		// Cache the returned deferred/promise by the id of the template
		// so that we can prevent multiple requests for the same template
		// from making the ajax call.
		//
		// This code is only safe to run synchronously. There exists a
		// race condition in this function, when run asynchronously,
		// which would nullify the benefit under certain circumstances.
		var loadTemplateAsync = function(templateName){			
			var promise = promises[templateName] || jQuery.get("/offline/js/templates/" + templateName + ".jst");
			promises[templateName] = promise;			
			return promise;
		}
		 
		// Use jQuery to asynchronously load the template.
		Backbone.View.prototype.getTemplate= function(templateName, callback){
			var self=this;
			if(typeof(templateName)=="function") 
			{
				callback=templateName;
				 templateName=self.templateName;
			}
			else if(templateName==undefined) templateName=self.templateName;			
			if(callback==undefined) callback=function(){};
			
			if(templateName==undefined) return;
			loadTemplateAsync(templateName).done(function(template){
				callback.call(self, template);
			});
			return self;
		}
	})(); */	
	
	
	
	return Controller;
});