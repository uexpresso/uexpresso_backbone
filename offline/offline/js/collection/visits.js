define(['../backbone','../Api', '../model/visits', 'view/visit'], function (Backbone, DbApi,  RequisitionVisit,  RequisitionVisitView) {
	
	var RequisitionVisitsList = Backbone.Collection.extend({
		model:RequisitionVisit,
		objectName: "RequisitionVisitsList",
		requisitionId:0,
		
		parse: function(response,options) {		
		
		   //perform your work on 'response', 
		   //    return the attributes this model should have.
		   return response.result;
		},
		sendAllways:["requisitionId"],
		appendListTo:function(jObject){		
			if(this.models.length==0) return;
			for(x in this.models)
			{
				var View=new RequisitionVisitView({model: this.models[x]});	
				$(View.render()).appendTo(jObject);
			}
		},
		
		addItemToList:function(itemJson){		
			var itemObj={};			
			if(itemJson!=undefined) itemObj=$.parseJSON(itemJson);			
			this.push(itemObj);
		}
	});	

return RequisitionVisitsList;
});