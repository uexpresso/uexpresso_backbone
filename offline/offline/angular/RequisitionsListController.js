fejerproApp.controller('RequisitionsListController', ['$scope', '$templateCache', 'DataLayer', 'ngDialog', 'Notification', function($scope, $templateCache, DataLayer, ngDialog, Notification) {
      $scope.maxCacheSize = 5;
      $scope.zebraTableIndex = 0;
      $scope.sortType = 'req_number';
      $scope.sortReverse = true;
      $scope.completion = [
          { name: 'uncompleted', value: 0 }, 
          { name: 'completed', value: 1 }
        ];

      $scope.filterType = {
        completion: $scope.completion[0].value,
        year: new Date().getFullYear().toString(),
        tour: ''
      };

      $scope.updateList = function () {
        DataLayer.getRequisitions($scope.filterType.year, function(data){
          $scope.requisitions = data.result.requisitions;
          $scope.tours = data.result.tours;
          $scope.years = data.result.years;
        });
      };

      $scope.updateList();

      $scope.getDetails = function() {
        for (var i = 0; i <= $scope.requisitions.length - 1; i++) {        
          DataLayer.getRequisitionDetails($scope.requisitions[i], function(response){});
        };     
      };

      $scope.getSize = function() {
        $scope.size = sizeof($scope.requisitions, true);
      };

      $scope.checkboxChanged = function(requisition) {
        if(requisition.details) {
          ngDialog.open({ 
            templateUrl: 'delete-from-cache-popup.html',
            controller: ['$scope', function($scope) {
                $scope.remove = function() 
                { 
                  requisition.details = undefined;
                };
                $scope.cancel = function() 
                { 
                  requisition.detailsLoaded = requisition.details !== undefined;
                };
            }]
          });
        }
        else {
          var totalCachedCount = $scope.requisitions.filter(function(requisition){return requisition.detailsLoaded === true}).length;
          if(totalCachedCount > $scope.maxCacheSize){
            Notification.error('Offline storage is full. Please free some space by removing any other Requisitions from the storage.');
            requisition.detailsLoaded = false;
          }
          else {
            DataLayer.getRequisitionDetails(requisition, function(response){});          
          }
        }
      };

/*
      $scope.filter = function () {
        if($scope.filterType.tours && $scope.filterType.tours.tour_id == null) //'non selected' & press filter
        {
          $scope.filterType.tours = undefined;
        }

        for(prop in $scope.filterType) {
          $scope.actualFilter[prop] = $scope.filterType[prop];
        }
      };

      $scope.resetFilter = function () {
        $scope.actualFilter = {};
        $scope.filterType.req_year = $scope.years[0];
        $scope.filterType.tours = undefined;
        $scope.filterType.is_completed = $scope.completion[0].value;
      };
*/

      $scope.resetFilter = function () {
        var s = $scope.filterType;
        debugger;
      };

      $scope.filterChanged = function () {
        $scope.updateList();
      };

      $scope.clearCache = function ()
      {
        DataLayer.clearCache();
      };

      $scope.requisitionFilter = function(requisition) {
        var filter = $scope.filterType;
        var isSelected = requisition.tours.some(item => item.tour_id === filter.tour) || filter.tour == '';
        isSelected = isSelected && requisition.req_year == filter.year 
        isSelected = isSelected && requisition.is_completed == filter.completion;
        return isSelected;
      };
  }]);

fejerproApp.directive('requisitionsTable', function () {
  return {
      restrict: 'A',
      templateUrl: 'requisitions-table.html'
  }
});