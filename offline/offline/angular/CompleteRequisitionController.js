var controller = fejerproApp.controller('CompleteRequisitionController', ['$scope', '$routeParams', '$templateCache', '$filter','DataLayer', 'ngDialog', 'Notification', function($scope, $routeParams, $templateCache, $filter, DataLayer, ngDialog, Notification) {
	DataLayer.getRequisitionDetailsById($routeParams.requisitionId, function(data){
		$scope.requisition = data;
	});
	$scope.display = {
		showFull: 1
	};

	$scope.showCompleted = true;

	$scope.filterType = {
		planned: '',
		completed: ''
	};

	$scope.plannedDate = $filter('date')(new Date().toISOString().substring(0, 10), 'MM/dd/yyyy');

	$scope.completionChanged = function() {
		if(!$scope.showCompleted)
		{
			$scope.filterType.completed = '0000-00-00';
			$scope.filterType.cancelled = '0';
		}
		else
		{
			$scope.filterType.completed = '';
			$scope.filterType.cancelled = '';
		}
	};

	$scope.editComment = function (sweeperPropId, comment)
	{
		$scope.sweeperPropId = sweeperPropId;
		if(comment == undefined)
		{
			$scope.creation = true;
			$scope.commentToEdit = {
				category: '',
				comment_id: 0,
				created: '',
				deadline: '',
				detail: '',
				requisition_id: 0,
				show_in_requisitions: true,
				show_in_next: false,
				this_requisition_only: '',
				title: ''
			}
		}
		else
		{
			comment.show_in_requisitions = comment.show_in_requisitions == 1;
			$scope.commentToEdit = angular.copy(comment);;
			$scope.creation = false;
		}

		ngDialog.open({ 
		templateUrl: 'edit-comment.html',
		className: ' ngdialog-theme-default comment-edit-box',
		scope: $scope
		});
	}

	$scope.getDays = function(startDate)
	{
		var timeSpan = new Date(startDate) - new Date();
		var days = timeSpan / (1000 * 60 * 60 * 24);
		return days+1;
	}

	$scope.handleSave = function ()
	{
		var commentContainer = $scope.requisition.details.filter(item => item && item.sweeper_property_id == $scope.sweeperPropId)[0];

		if($scope.commentToEdit.comment_id == 0)
		{
			commentContainer.comments.push($scope.commentToEdit);
		}
		else
		{
			var index = commentContainer.comments.map(function(c) { return c.comment_id; }).indexOf($scope.commentToEdit.comment_id);
			if(index != -1)
				commentContainer.comments[index] = $scope.commentToEdit;
		}

		DataLayer.updateComment($scope.sweeperPropId, $scope.commentToEdit, function (callbackComment) {
			if(callbackComment.comment_id == 0) //For new comment get it's id from the server
			{
				if(index != -1)
				commentContainer.comments[index] = callbackComment;
			}
		});
	}

	$scope.handleDelete = function (comment)
	{
		var commentContainer = $scope.requisition.details.filter(item => item && item.comments.some(comment => comment.comment_id == $scope.commentToEdit.comment_id))[0];	
		var index = commentContainer.comments.map(function(c) { return c.comment_id; }).indexOf($scope.commentToEdit.comment_id);
		if (index > -1) {
		    commentContainer.comments.splice(index, 1);
		}

		DataLayer.deleteComment($scope.sweeperPropId, $scope.commentToEdit, function (callbackComment) {
			$scope.commentToEdit = undefined;
		});
	}
}]);

controller.directive('networkUrl', ['$window', '$http', 'Notification', function($window, $http, Notification) {
  return function(scope, element, attrs) {
  var checkAvailability = function() {
    $http.head(attrs.networkUrl)
    .then(function successCallback(response) {
      $window.location.href = attrs.networkUrl;
    }, function errorCallback(response) {
      Notification.error({message:'Network is not available. Please try again later.'});
    });
  };
  element.bind('click', checkAvailability);
  }
}]);

controller.directive('propertyList', function () {
  return {
      restrict: 'A',
      templateUrl: 'property-list.html'
  }
});