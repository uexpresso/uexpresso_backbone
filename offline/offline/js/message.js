/*
	message.js - jQuery Plugin
	
	AUTHOR: Andriy Slobodyan, Web-Developer, ErKi Data Aps.
	
	DESCRIPTION: display message in fine centered block
	
	HOW TO USE: 
		Step 1. Include jQuery library
		Step 2. Include message.min.js Plugin
		Step 3.	Use aslobodyanMessage(TRUE,MESS,DETAILS,function(){});  where:
				
				TRUE - 	this parameter indicates to the plugin to show or not to show the button "Yes! I'm sure."
						If there is no need to show this button just miss this parameter.
				MESS - your message, the only REQUIRED parameter.
				DETAILS - associative array of submessages.
				function() - this function will fire when additional functionality is needed
	
	ATTENTION!
		This Plugin is for free, not for sale.
		You have the right to modify the plugin script and even its functionality according to your needs,
		but you have no right to modify the name of the main plugin's function aslobodyanMessage().
		Also you have no right to change or delete the name of the plugin's author (Andriy Slobodyan) from this document.
*/
(function($){
	$.fn.mess=function(isBut,m,descr,func){
		return this.each(function(){
			if($('#messCont')) $('#messCont').html('').remove();
			if($('#messBg'))$('#messBg').remove();
			if($('body'))$('body').removeAttr('style');
			if(typeof(isBut)=='boolean'){
				var isButton=true;
				var message=m
			} else if(typeof(isBut)=='string'){
				var isButton=false;
				var message=isBut
			}
			if(typeof(m)=='object')var detailArr=m;
			else if(typeof(descr)=='object')var detailArr=descr;
			var bg=$('<div />',{
				id:'messBg',
				style:'height: '+$(document).height()+'px'
			});
			$('body').css({
				height:$(document).height()+'px',
				overflow:'hidden',
				position:'relative'
			}).prepend(bg);
			var messCont=$('<div />',{
				id:'messCont',
				style:'top: '+screenY()+'px; left:'+screenX()+'px;'
			});
			$('body').append(messCont);
			var messClose=$('<a />',{
				href:'javascript:void(0)',
				title:'',
				id:'messClose',
				style:'outline: 0 none;'
			});
			$('#messCont').append(messClose);
			$('#messClose').text('luk [x]');
			var messDiv=$('<div />');
			$('#messCont').append(messDiv);
			$('#messCont div').append('<h2>'+message+'</h2>');
			if(detailArr!='undefined')
				for(var i in detailArr)
					$('#messCont div').append('<h3>'+detailArr[i]+' (<span style="text-transform:capitalize;">'+i+'</span>)</h3>');
			if(isButton){
				$('#messCont div').append('<a href="javascript:void(0)" class="btnBlue" id="messYes"></a>');
				$('#messYes').text("Ja! Jeg er sikker.")
			} else {
				$('#messYes').remove(this)
			}
			centerMess();
			$(window).bind('resize',function(){
				centerMess()
			});
			$('#messClose').bind('click',function(){
				$('#messCont').html('').remove();
				$('#messBg').remove();
				$('body').removeAttr('style')
			});
			if(typeof(m)=='function') m();
			else if(typeof(descr)=='function') descr();
			else if(typeof(func)=='function') func()
		})
	}
})(jQuery);
function aslobodyanMessage(a,b,c,f){
	jQuery('body').mess(a,b,c,f)
};
function centerMess(){
	var asContH='-'+parseInt(jQuery('#messCont').height())*0.5+'px';
	var asContW='-'+parseInt(jQuery('#messCont').width())*0.5+'px';
	jQuery('#messCont').css({
		margin:asContH+' 0 0 '+asContW,
		top:screenY()+'px',
		left:screenX()+'px'
	})
};
function screenX(){
	return parseInt(window.getClientWidth()*0.5)+window.getBodyScrollLeft()
};
function screenY(){
	return parseInt(window.getClientHeight()*0.5)+window.getBodyScrollTop()
};
function getClientWidth(){
	return document.compatMode=='CSS1Compat'&&!window.opera?document.documentElement.clientWidth:document.body.clientWidth
};
function getClientHeight(){
	return document.compatMode=='CSS1Compat'&&!window.opera?document.documentElement.clientHeight:document.body.clientHeight
};
function getBodyScrollTop(){
	return self.pageYOffset||(document.documentElement&&document.documentElement.scrollTop)||(document.body&&document.body.scrollTop)
};
function getBodyScrollLeft(){
	return self.pageXOffset||(document.documentElement&&document.documentElement.scrollLeft)||(document.body&&document.body.scrollLeft)
}