define(['../backbone','../Api', 'model/requisition', 'view/requisition'], function (Backbone, DbApi,  Requisition, RequisitionView) {

	var RequisitionsList = Backbone.Collection.extend({
		model:Requisition,
		objectName: "RequisitionsList", /** unused. for debug only **/
		table:"requisitions",	/** REQUIRED for refresh() method specified in Api.js **/
		order:'desc',  /** hardcoded default order for lists **/
		parse: function(response,options) {		
		
		   //perform your work on 'response', 
		   //    return the attributes this model should have.
		  /** responce.result is a common thing in Fejerpro requests system **/
		   return response.result;
		},
		
		/** this used to render each model to some HTML node. in this case hardcoded to table **/
		appendListTo:function(jObject){		
			if(this.models.length==0) return;
			
			/** tasty testy progressbar **/
			 jQuery( "#progressbar" ).show().progressbar({value: 0});
			 
			 var totalLength=this.models.length;
			 var currentresult=0;
			var self=this;
			for(x in this.models)
			{
				new Promise(function(resolve, reject)
				{
				var View=new RequisitionView({model: self.models[x]});	
				
				View.render(totalLength);	
				resolve(View); }).then(function(View){
				$(View.el).appendTo(jObject);
				});
			}
			
			
		},
		/** add a new model by JSON. can be moved to Api for universality. unused though**/
		addItemToList:function(itemJson){		
			var itemObj={};			
			if(itemJson!=undefined) itemObj=$.parseJSON(itemJson);			
			this.push(itemObj);
		},
		/** hardcoded crappy crap to get requisiton models from db if available or from server and render their <tr>s to table **/
		render:function(){
			jQuery('#requisitionsTable tbody').empty();
			var Collection=this;
			
			DbApi.count('requisitions').done(function(result){
				if(result==0)
				{
					jQuery('.layout.content').prepend('<p>No requisitions in database. Getting from server....</p>');
					Collection.fetch({success:function(){
							var modelArray=[];
							Collection.appendListTo('#requisitionsTable tbody');
							for(x in Collection.models)
							{
								Collection.models[x].placeEditButton();
								modelArray.push(Collection.models[x].toJSON());
								
								
							}
							
							DbApi.add('requisitions', modelArray).done;							
						}
					});
				}
				else
				{
					jQuery('.layout.content').prepend('<p>Requisitions in database: '+result+' Showing table...</p>');
			
					DbApi.values('requisitions', undefined, result).done(function(requisitionsResult){
					
						Collection.add(requisitionsResult); 	
						
						Collection.appendListTo('#requisitionsTable tbody');
					
					
					});
				}
			});
			
			
			
		}
	});

var RequisitionsList=new RequisitionsList();


return RequisitionsList;
});