define(function(){
    var instance = null;

    function Lang(){
        if(instance !== null){
            throw new Error("Cannot instantiate more than one Lang, use Lang.getInstance()");
        } 
        
        this.initialize();
    }
    Lang.prototype = {
        initialize: function(){
            // summary:
            //      Initializes the singleton.
           
        },
		dictionaries:[],
		
		translateHtml:function(html, lang, callback){
				var self=this;									
				if(self.dictionaries.length==0 || self.dictionaries[lang]==undefined)
				{
					
					var langHref='/lang/getLang.php';
					langHref=langHref+'?lang='+lang;								
					jQuery.ajax({url:langHref, type:'get', async:true, success:function(data){
						var dictionaries=JSON.parse(data);
						if(self.dictionaries["basic"]==undefined) self.dictionaries["basic"]=dictionaries;
						self.dictionaries[lang]=dictionaries;
						if(dictionaries.basic!=undefined) self.dictionaries.basic=dictionaries.basic;
						html=html.replace(/\{#([\w\s]+)\#}/g, function (match) { 
							var term=match.replace('{#','').replace('#}','');
							if(dictionaries[term]!=undefined) match=dictionaries[term];
							return match;
						});
						if(callback!=undefined) callback.call(self, html);
					}});	
				}
				else
				{
					html=html.replace(/\{#([\w\s]+)\#}/g, function (match) { 
							var term=match.replace('{#','').replace('#}','');
							if(self.dictionaries.basic[term]!=undefined) match=self.dictionaries.basic[term];
							return match;
					}).replace(/\{#([\w\s]+)\#}/g, function (match) { 
							var term=match.replace('{#','').replace('#}','');
							if(self.dictionaries[lang][term]!=undefined) match=self.dictionaries[lang][term];
							return match;
					});
					if(callback!=undefined) callback.call(this, html);
				}
		}
    };
    Lang.getInstance = function(){
        // summary:
        //      Gets an instance of the singleton. It is better to use 
        if(instance === null){
            instance = new Lang();
        }
        return instance;
    };

    return Lang.getInstance();
});

