/**
	Dependency loader
**/
requirejs.config({
			shim: {
				'../offline/js/backbone': {
					deps: ['../offline/js/lodash', '../js/jquery-1.7.1'], // load dependencies
					exports: 'Backbone' // use the global 'Backbone' as the module value
				},
				'../offline/js/Lang' : { exports: 'Lang'}
			}
		});

		
		
/**
	MAIN BACKBONE API RUN
**/		
require(['Api', 'Lang'], function (Api, Lang) {

/**
	this part is totally hardcoded for requisitions only page.
	for other templates I'll have to make something more universal or use different data-main js file
**/


/**
	to read all backbone parts spagetty start with /collection/requisition.js
	it will also include /model/requisition.js and /view/requisition.js
	
	model run will start requisition visits collection found in /collection/visits.js then /model//collection/visits.js and /view//collection/visits.js
	
	all filenames are hardcoded
	no real way to use any magic methods to load classes here
**/


/**
	most universal script - Api
	it keeps methods that can be used for any collection
	and also ydnDb interfaces
	
	for any changes to DB model (add table, create index, etc.) you have to change db schema in Api file
	that's just how crappy IndexedDb is
**/

window.Api=Api;

	/**
		running templater
	**/
	
	jQuery('template').each(function(){		
		var templateTag=jQuery(this);		
		var route=jQuery(this).html().split('/');
		
		if(route[0]!=undefined) var controllerName=route[0];
		if(route[1]!=undefined) var userFunction=route[1]; else var userFunction='defaultFunction()';
		if(controllerName!=undefined)
		{
			require(['controller/'+controllerName],function(Controller){				
				if(typeof(Controller[userFunction])=='function') Controller[userFunction]();
				else console.log(userFunction);
				
			});
		}
		else
		{
			jQuery.get('/offline/js/templates/'+jQuery(templateTag).html()+'.jst',function(html){
			
				Lang.translateHtml(html, jQuery(templateTag).attr('lang'), function(html){ 
					jQuery(templateTag).replaceWith(html); 
				});
					
			});
		}
	});

/*
jQuery('#refresh').click(function(){
	RequisitionsList.refresh(DbApi);
});
*/

/**
	autoupdater run
**/
//var updateTimer=setInterval(function(){RequisitionsList.refresh(DbApi);},3000);

	
});

