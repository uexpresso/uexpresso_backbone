define(['../Controller'],function (ControllerAbstract) {
	
		// I return an initialized object.
		function Controller(){
			// Call the super constructor.
			ControllerAbstract.call( this );
			// Return this object reference.
			
			return( this );
		}
		// The Controller class extends the base ControllerAbstract class.
		Controller.prototype = Object.create( ControllerAbstract.prototype );
		var Controller=new Controller();
		Controller.lang='requisitions';
		
		// -------------------------------------------------- //
		// -------------------------------------------------- //
		
		Controller.showList=function(){
			console.time('showlistStart');
			console.time('showlistEnd');
			console.time('Api.count');
			console.time('Api.values');
			console.time('require');
			this.template('RequisitionsList_page');
			var self=this;
			Api.count('requisitions').done(function(result){
				console.timeEnd('Api.count');
				console.log(result);
				if(result==0)
				{
					jQuery('.layout.content').prepend('<p>No requisitions in database. Getting from server....</p>');
					self.request('/backbone/requisitionsList/action,show/request,ajax/mode,json/').done(function(data){
						if(data!=undefined)  if(data.result!=undefined && data.result.length>0)
						 {
							Api.add('requisitions', data.result).done;	
							self.renderRequisitionsList(data.result);
						 }
					});
					/*
					Collection.fetch({success:function(){
							var modelArray=[];
							Collection.appendListTo('#requisitionsTable tbody');
							for(x in Collection.models)
							{
								Collection.models[x].placeEditButton();
								modelArray.push(Collection.models[x].toJSON());
								
								
							}
							
							DbApi.add('requisitions', modelArray).done;							
						}
					});*/
				}
				else
				{
					Api.values('requisitions', undefined, result).done(function(requisitionsResult){
						self.renderRequisitionsList(requisitionsResult);	
					});
				}
			});
			console.timeEnd('showlistStart');
		}
		
		Controller.renderRequisitionsList=function(requisitionsResult)
		{
			console.timeEnd('Api.values');
					require(['model/requisition','text!templates/RequisitionsList_node.jst'], function (Requisition_Entity, pageLineTemplate) {
					console.timeEnd('require');
				
						var template=_.template(pageLineTemplate);
						for(x in requisitionsResult)
						{
							var Requisition=new Requisition_Entity(requisitionsResult[x]);							
						
								template(Requisition.attributes);							
								
							jQuery('#requisitionsTable tbody').prepend(Requisition.View);
						
							Requisition.prependObjectTo(jQuery('#requisitionsTable tbody'));
						
						}	
						console.timeEnd('showlistEnd');
						jQuery('#requisitionsTable tbody .preloader').remove();
					});
					
					/*require(['collection/requisitions'], function (RequisitionsList) {	 		
						RequisitionsList.add(requisitionsResult); 	
						
						RequisitionsList.appendListTo('#requisitionsTable tbody');
					});
					*/
		}
		
		return  Controller;

});