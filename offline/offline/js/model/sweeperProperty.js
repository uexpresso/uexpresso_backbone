define(['../backbone','../Api'], function (Backbone) {

	var SweeperProperty= Backbone.Model.extend({
		defaults:function(){
			return {
				id  	    	:'id',			
				propertyLocation		:'location',
				propertyPosition		:'position',
				bbrCommune	:'bbr_commune_code',
				bbrProperty	:'bbr_property_number',
				bbrCode		:'bbr_code',
				zip			:'zip',
				address		:'address',
				house			:'house',
				floor			:'floor',
				doorNumber	:'door_number',
				matrikelnr	:'matrikelnr',
				ejetlav		:'ejetlav',
				placename		:'placename',
				tourGroup		:'tour_group',
				propertyType	:'property_type',
				paymentType	:'payment_type',
				billDelivery	:'bill_delivery_type',
				isTaxpaid		:'is_taxpaid',
				visits		:'visits',
				saldo			:'saldo',
				detail		:'detail',
				gathering 	:'gathering_id',
				mainPropertyID :'main_property_id',
				keyNumber		:'key_number',
				payerNotification		:'notif_payer',
				isCollection	:'is_collection',
				collectorId	:'collector_id'
			};
		},
		objectName: "SweeperProperty",
		sync:function(){ return false; },		
		logThis:function(){console.log(this);}
	});


return SweeperProperty;
});